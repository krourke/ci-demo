const chai = require('chai');
const asPromised = require('chai-as-promised');
const { Calculator, PI } = require('../calc');

chai.use(asPromised);

const { assert } = chai;

describe('Calculator', () => {
  const calc = new Calculator();

  describe('#addLater()', () => {
    it('Can eventually add positive integeters', async () => {
      assert.eventually.equal(calc.addLater(0, 0), 0);
      assert.eventually.equal(calc.addLater(1, 42), 43);
    });

    it('Can eventually perform subtraction', async () => {
      assert.eventually.equal(calc.addLater(56, -13), 43);
      assert.eventually.equal(calc.addLater(0, -42), -42);
    });
  });

  describe('#factorial()', () => {
    it('Can factorialize 0', () => {
      assert.equal(calc.factorial(0), 1);
    });

    it('Can cache results', () => {
      assert.equal(calc.factorial(10), 3628800);
      assert.propertyVal(calc._fact_cache, '10', 3628800);
    });

    it('Throws if input is not integer', () => {
      assert.throws(() => calc.factorial(PI), 'Must be integer!');
    });
  });
});
