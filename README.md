# ci-demo

This is a simple JavaScript project that demonstrates GitLab CI with:

 - Lint and test stages
 - Dependency caching

## Linting

Code linting is acheived using `eslint`. See `.eslintrc.yml` for more
info.

## Testing

Tests are run using the `mocha` framework with the Chai assertion
library. The included tests demonstrate some features like:

 - Nested suites
 - Sychronous tests
 - Asychronous tests
 - Assertions
 - Exception handling

### Coverage

Test coverage is reported with the `nyc` reporter.

This project sets a permissive test coverage threshold.
