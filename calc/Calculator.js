/** @module calc */
/**
 * A calculator with synchronous and asynchronous methods.
 *
 * This is an example of an ES6 class, which is syntactically more friendly
 * than prototype-based classes.
 *
 * @property {number} delay How long the timer should wait
 */
class Calculator {
  /**
   * Creates an instance of Calculator.
   *
   * @param {number} delay Override the default delay of 200ms
   * @memberof Calculator
   */
  constructor(delay) {
    this._delay = delay || 200;
    this._fact_cache = {
      0: 1,
    };
  }

  get delay() {
    return this._delay;
  }

  set delay(val) {
    this._delay = val;
  }

  /**
   * Adds a and b after some delay.
   *
   * @param {number} a The first number
   * @param {number} b The second number
   * @returns {number} The sum of a and b.
   * @memberof Calculator
   */
  async addLater(a, b) {
    const result = await new Promise((resolve) => {
      setTimeout(() => resolve(a + b), this.delay);
    });
    return result;
  }

  /**
   * Multiplies a by b after some delay.
   *
   * @param {number} a The first number
   * @param {number} b The second number
   * @returns {number} The product of a and b.
   * @memberof Calculator
   */
  async multiplyLater(a, b) {
    const result = await new Promise((resolve) => {
      setTimeout(() => resolve(a * b), this.delay);
    });
    return result;
  }

  /**
   * Caching factorial function.
   *
   * @param {number} n The number to factorialize
   * @returns {number} n!
   * @memberof Calculator
   */
  factorial(n) {
    if (!Number.isInteger(n)) {
      throw TypeError('Must be integer!');
    }
    if (this._fact_cache[n] != null) {
      return this._fact_cache[n];
    }
    let result = 1;
    for (let i = 1; i <= n; i++) {
      result *= i;
    }
    this._fact_cache[n] = result;
    return result;
  }
}

module.exports = Calculator;
