/** @module calc */

const Calculator = require('./Calculator');

const PI = 3.1415926;

module.exports = {
  Calculator,
  PI,
};
